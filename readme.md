# Analysis of "robust" (penalized) least-squares

### Abstract
In supervised learning, training a predictor often means minimizing an empirical risk, corresponding to the error evaluated on a training dataset. This corresponds to a minimization problem often solved resorting to an iterative algorithm. The resulting predictor is expected to perform well on the training set, but overall  to be able to give good predictions on new data --- often referred to as generalization. The former is actually challenged when the risk function to be minimized is non-convex. To increase the quality of the found minimizer, practitioners have introduced Sharpness-Aware Minimization (SAM) [Foret et al., 2020].
We study its connections to robust optimization in the particular case of linear regression.

### Acknowledgments
I would first like to express all my profound gratitude to my supervisors Claire Boyer (LPSM, Sorbonne Université) and Maxime Sangnier (LPSM, Sorbonne Université). They have guided me during the whole internship, deeply helping me in the redaction of the following report and throughout every thought process.
I would also like to extend my warmest thanks to Guillaume Garrigos (LPSM, Université Paris Cité) who participated in most debriefs and became an informal supervisor thanks to the relevant knowledge he brought to the discussions.
Finally, I thank all members of LPSM for the kind and pleasant environment I evolved in for the past months.