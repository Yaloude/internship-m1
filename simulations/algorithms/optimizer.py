import numpy as np
from typing import Iterable, Iterator, Callable, Union, Optional

class Optimizer:
    def __init__(
            self,
            g_func: Callable,
            lr: Union[Iterable, Iterator, Callable, float]) -> None:
        super().__init__()
        self.lr = None
        self.iterator_lr = None
        self.callable_lr = None

        if isinstance(lr, np.ndarray) and lr.shape == ():
            lr = lr.item()

        if isinstance(lr, Iterable):
            self.iterator_lr = iter(lr)
        elif isinstance(lr, Iterator):
            self.iterator_lr = lr
        elif isinstance(lr, Callable):
            self.callable_lr = lr
        elif isinstance(lr, (int, float)):
            self.lr = lr
        else:
            raise ValueError("Illegal type for learning rate.")

        self.g_func = g_func
        self.k = -1

    def get_lr(self) -> Optional[float]:
        if self.iterator_lr is not None:
            return next(self.iterator_lr)
        if self.callable_lr is not None:
            self.k += 1
            return self.callable_lr(self.k)
        if self.lr is not None:
            return self.lr
        return None

    def step(self, betas: np.ndarray, *args, **kwargs) -> np.ndarray:
        pass

