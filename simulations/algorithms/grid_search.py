import numpy as np
from typing import Tuple, Callable, Any, Iterable, Iterator
from numpy.typing import ArrayLike

def grid_search(
        score_func: Callable,
        values: ArrayLike,
        mode: str = "min") -> Tuple[Any, np.ndarray]:
    """Performs an iterative grid search.
    Compares the score of :score_func: of every value in :values: 

    Args:
        score_func: Callable
            A callable that returns the score for a given value.
        values: array_like
            All values on which the grid search is done.
        mode: {'min', 'max'}
            If mode is 'max' then the hyper-parameters with the best score is taken ; 
             otherwise it is the one with the lowest score. (default 'max')

    Returns:
        Returns the best value and all the scores in a tuple.
    """
    assert mode in {"min", "max"}, "Parameter ``mode`` must be either 'min' or 'max'."

    shape = values.shape[:-1]
    if values.ndim > 1:
        values = values.reshape(values.size // values.shape[-1], values.shape[-1])
    else:
        values = np.expand_dims(values, -1)

    scores = np.array([score_func(*v) for v in values])
    idx_best = scores.argmin() if mode == "min" else scores.argmax()
    best_value = values[idx_best].squeeze()

    if shape != ():
        scores = scores.reshape(shape).squeeze()

    return best_value, scores