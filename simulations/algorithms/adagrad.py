import numpy as np
from .shared import run_iterative
from .optimizer import Optimizer
from typing import Callable, Optional, Iterable, Iterator, Union, Tuple


class Adagrad(Optimizer):
    def __init__(self, g_func: callable, lr: float, eps: float = 1e-8):
        super().__init__(g_func, lr)
        self.eps = eps
        self.grad_sum = 0

    def step(self, betas: np.ndarray, *args, **kwargs) -> np.ndarray:       
        lr = self.get_lr()
        grad = self.g_func(betas)
        self.grad_sum += np.square(grad)

        extended_grad = grad / (np.sqrt(self.grad_sum) + self.eps)
        # extended_grad = grad / (np.abs(grad) + self.eps)
        new_betas = betas - lr * extended_grad
        return new_betas

def adagrad(
        n_max: int,
        beta0: np.ndarray,
        l_func: Callable,
        g_func: Callable,
        step_size: float) -> np.ndarray:
    """Implements the Adaptive Gradient (AdaGrad) algorithm.

    Args:
        n_max : int
            Maximum number of iterations of the algorithm.
        beta0 : array_like
            Initialization of the algorithm.
        l_func : Callable
            Loss function, used for early stopping.
        d_func : Callable
            Gradient of the loss function.
        step_size : float
            Step size(s) to use.

    Returns:
        Returns all of the iterates of at most :n_max: steps of the SAM algorithm and the loss over time.
    """
    opt = Adagrad(g_func, step_size)
    return run_iterative(n_max, beta0, l_func, opt)