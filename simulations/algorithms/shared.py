import numpy as np
from .optimizer import Optimizer
from typing import Callable, Optional, Iterable, Iterator, Union, Tuple

def run_iterative(        
        n_max: int,
        beta0: np.ndarray,
        l_func: Callable,
        opt: Optimizer) -> np.ndarray:
    """Run an iterative algorithm whose steps are in :algo_step:.

    Args:
        n_max : int
            Number of iterations of the algorithm.
        beta0 : array_like
            Initialization of the algorithm.
        l_func : Callable
            Loss function, used for early stopping.

    Returns:
        Returns all of the iterates of n steps of the SGD algorithm and the loss over time.
    """
    current_betas = beta0.copy()
    if current_betas.ndim <= 1:
        current_betas = np.expand_dims(current_betas, 0)

    N, d = current_betas.shape[0], current_betas.shape[1]

    betas = np.empty((1, N, d))
    betas[0] = current_betas

    losses = np.empty((1, N))
    losses[0] = l_func(current_betas)

    k = 0
    while k < n_max and np.any(losses[-1] > 1e-16):
        current_betas = opt.step(current_betas)
        betas = np.append(betas, [current_betas], axis=0)
        losses = np.append(losses, [l_func(current_betas)], axis=0)
        current_betas = current_betas
        k += 1

    betas = np.swapaxes(betas, 0, 1)
    losses = np.swapaxes(losses, 0, 1)
    return betas, losses