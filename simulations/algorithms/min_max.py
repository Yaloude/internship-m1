import numpy as np
from .shared import run_iterative
from .sgd import SGD
from .optimizer import Optimizer
from typing import Callable, Optional, Iterable, Iterator, Union, Tuple


class MinMaxLeastSquares(Optimizer):
    def __init__(self, g_func: Callable, lr: float, rho: float):
        super().__init__(g_func, lr)
        self.rho = rho

    def step(self, betas: np.ndarray, *args, **kwargs) -> np.ndarray:
        lr = self.get_lr()
        new_betas = betas - lr * self.g_func(betas, self.rho, *args, **kwargs)
        return new_betas


def min_max(
        n_max: int,
        beta0: np.ndarray,
        l_func: Callable,
        g_func: Callable,
        step_size: float,
        rho: float) -> np.ndarray:
    """Implements an alternate Min-Max algorithm.

    Args:
        n_max : int
            Maximum number of iterations of the algorithm.
        beta0 : array_like
            Initialization of the algorithm.
        l_func : Callable
            Loss function, used for early stopping.
        d_func : Callable
            Gradient of the loss function.
        step_size : float
            Step size to use for the outer gradient (descent step).
        rho : float
            Step size to use for the inner gradient (ascent step).

    Returns:
        Returns all of the iterates of n steps of the SGD algorithm and the loss over time.
    """
    opt = MinMaxLeastSquares(g_func, step_size, rho)
    return run_iterative(n_max, beta0, l_func, opt)