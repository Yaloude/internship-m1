import numpy as np
from .shared import run_iterative
from .optimizer import Optimizer
from typing import Callable, Optional, Iterable, Iterator, Union, Tuple


class SGD(Optimizer):
    def __init__(self, g_func: callable, lr: float):
        super().__init__(g_func, lr)    

    def step(self, betas: np.ndarray, *args, **kwargs) -> np.ndarray:
        lr = self.get_lr()
        grad = self.g_func(betas)
        new_betas = betas - lr * grad
        return new_betas


def sgd(
        n_max: int,
        beta0: np.ndarray,
        l_func: Callable,
        g_func: Callable,
        step_size: Union[float, Iterable, Iterator, Callable]) -> np.ndarray:
    """Implements the (Stochastic) Gradient Descent algorithm.

    Args:
        n_max : int
            Maximum number of iterations of the algorithm.
        beta0 : array_like
            Initialization of the algorithm.
        l_func : Callable
            Loss function, used for early stopping.
        d_func : Callable
            Gradient of the loss function.
        step_size : Union[float, Iterable, Iterator, Callable]
            Step size(s) to use.

    Returns:
        Returns all of the iterates of n steps of the SGD algorithm and the loss over time.
    """
    opt = SGD(g_func, step_size)
    return run_iterative(n_max, beta0, l_func, opt)