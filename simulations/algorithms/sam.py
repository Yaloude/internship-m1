import numpy as np
from .shared import run_iterative
from .optimizer import Optimizer
from typing import Callable, Optional, Iterable, Iterator, Union, Tuple


class SAM(Optimizer):
    def __init__(self, g_func: callable, lr: float, rho: float, normalize: bool = True):
        super().__init__(g_func, lr)
        self.rho = rho
        self.normalize = normalize

    def inner_step(self, betas: np.ndarray, *args, **kwargs) -> np.ndarray:
        inner_grad = self.g_func(betas)
        if self.normalize:
            inner_grad /= np.linalg.norm(inner_grad, axis=1)[..., None]

        inner_betas = betas + self.rho * inner_grad
        return inner_betas

    def step(self, betas: np.ndarray, *args, **kwargs) -> np.ndarray:
        lr = self.get_lr()
        inner_betas = self.inner_step(betas, *args, **kwargs)

        grad = self.g_func(inner_betas)
        new_betas = betas - lr * grad
        return new_betas

def sam(
        n_max: int,
        beta0: np.ndarray,
        l_func: Callable,
        g_func: Callable,
        step_size: float,
        rho: float,
        normalize: bool = True) -> np.ndarray:
    """Implements the Sharpness-Aware Minimization (SAM) algorithm.

    Args:
        n_max : int
            Maximum number of iterations of the algorithm.
        beta0 : array_like
            Initialization of the algorithm.
        l_func : Callable
            Loss function, used for early stopping.
        d_func : Callable
            Gradient of the loss function.
        step_size : float
            Step size(s) to use for the outer gradient.
        rho : float
            Step size(s) to use for the inner gradient.
        normalize: bool
            If true the used algorithm is SAM ; otherwise SAM-U (unnormalized SAM).

    Returns:
        Returns all of the iterates of at most :n_max: steps of the SAM algorithm and the loss over time.
    """
    opt = SAM(g_func, step_size, rho, normalize=normalize)
    return run_iterative(n_max, beta0, l_func, opt)