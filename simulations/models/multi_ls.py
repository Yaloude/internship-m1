import torch
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import lightning.pytorch as pl
from typing import Tuple, Optional


class MultiLeastSquares(pl.LightningModule):
    def __init__(self, d: int, n_estimators: int, lr: float) -> None:
        super().__init__()
        self.save_hyperparameters()
        self.lr = lr
        self.d = d
        self.n_estimators = n_estimators

        self.last_batch = None

        self.betas = nn.Parameter(torch.empty(self.n_estimators, self.d))
        self.init_weights()

    @torch.no_grad()
    def init_weights(self) -> None:
        self.betas.normal_(std=20)

    @torch.no_grad()
    def init_weights_around(self, around: Tensor) -> None:
        self.init_weights()
        self.betas += around

    @torch.no_grad()
    def get_coordinates(self) -> Optional[Tensor]:
        if self.last_batch is None:
            return None

        _, _, _, _, Vh = self.last_batch
        proj_betas = (Vh @ self.betas.T).mT.squeeze()
        return proj_betas

    @torch.no_grad()
    def get_residuals(self) -> Optional[Tensor]:
        if self.last_batch is None:
            return None

        X, y, U, _, _ = self.last_batch
        yHat = self(X).squeeze()
        y = y.repeat(yHat.size(0), 1)
        res = y - yHat
        tilde_x = (U.mT @ res.T).mT.squeeze()
        return tilde_x

    def get_residuals_ratio(self) -> Optional[Tensor]:
        res = self.get_residuals()
        if res is None:
            return None

        sq = res.square()
        res_ratio = sq[..., 0]/sq[..., -1]
        return res_ratio

    def forward(self, X: Tensor) -> Tensor:
        return (X @ self.betas.T).mT

    def training_step(self, batch: Tuple[Tensor], batch_idx: int) -> Tensor:
        self.last_batch = batch
        X, y, _, _, _ = batch
        yHat = self(X).squeeze()
        y = y.repeat(yHat.size(0), 1)
        # loss = (y - yHat).square().sum()
        loss = F.mse_loss(yHat, y, reduction='sum')
        self.log("loss", loss, on_epoch=True, on_step=False, prog_bar=True)
        return loss

    # def on_train_epoch_end(self) -> None:
    #     coords_ratio = self.get_coordinates_ratio()
    #     if self.save_all_coords_ratio:
    #         for k, ratio in enumerate(coords_ratio):
    #             self.log(f"{COORDS_RATIO_KEY_PREFIX}{k}", ratio, on_epoch=True, on_step=False)
    #     self.log(MEAN_COORDS_RATIO_KEY, coords_ratio.mean(), on_epoch=True, on_step=False, prog_bar=True)

    def configure_optimizers(self) -> torch.optim.Optimizer:
        optimizer = torch.optim.SGD(self.parameters(), lr=self.lr) # Empirical learning rate
        return optimizer