import torch
from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
import lightning.pytorch as pl
from typing import Tuple, Optional


class MultiQuadratic(pl.LightningModule):
    def __init__(self, n: int, n_estimators: int, lr: float) -> None:
        super().__init__()
        self.save_hyperparameters()
        self.lr = lr
        self.n = n
        self.n_estimators = n_estimators

        self.betas = nn.Parameter(torch.empty(self.n_estimators, self.n))
        self.init_weights()

    @torch.no_grad()
    def init_weights(self) -> None:
        self.betas.normal_()

    @torch.no_grad()
    def get_residuals_ratio(self) -> Tensor:
        sq = self.betas.square()
        coords_ratio = sq[..., 0]/sq[..., 1]
        return coords_ratio

    def forward(self, X: Tensor) -> Tensor:
        return self.betas * X

    def training_step(self, batch: Tuple[Tensor, ...], batch_idx: int) -> Tensor:
        X, _ = batch
        yHat = self(X)
        loss = yHat.square().sum() / 2 # .mean() makes too small steps
                            # Consider that each column of betas is an independant estimator
        self.log("loss", loss, on_epoch=True, on_step=False, prog_bar=True)
        return loss

    # def on_train_epoch_end(self) -> None:
    #     coords_ratio = self.get_coordinates_ratio()
    #     self.log(MEAN_COORDS_RATIO_KEY, coords_ratio.mean(), on_epoch=True, prog_bar=True)

    def configure_optimizers(self) -> torch.optim.Optimizer:
        optimizer = torch.optim.SGD(self.parameters(), lr=self.lr) # Empirical learning rate
        return optimizer
