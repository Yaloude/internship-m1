from .multi_quadratic import MultiQuadratic
from .multi_ls import MultiLeastSquares

__all__ = [
    "MultiQuadratic", "MultiLeastSquares"
]