import torch
from torch import Tensor
import lightning.pytorch as pl

class ResidualsRatioLoggerCallback(pl.Callback):
    def __init__(self,
                 key_prefix: str = "residuals_ratio_",
                 method_name: str = "get_residuals_ratio",
                 log_first_n_ratios: int = 0,
                 check_finite: bool = True) -> None:
        super().__init__()
        self.key_prefix = key_prefix
        self.log_first_n_ratios = log_first_n_ratios
        self.method_name = method_name
        self.check_finite = check_finite

    def _should_skip(self, trainer: pl.Trainer) -> bool:
        from pytorch_lightning.trainer.states import TrainerFn
        if trainer.state.fn != TrainerFn.FITTING or trainer.sanity_checking:
            return True
        return False

    def _should_stop(self, value: Tensor) -> bool:
        if self.check_finite and torch.any(torch.isnan(value) | torch.isinf(value)).item() == True:
            return True
        return False

    def on_train_epoch_end(self, trainer: pl.Trainer, pl_module: pl.LightningModule) -> None:
        if self._should_skip(trainer):
            return

        if self.method_name not in dir(pl_module):
            raise Exception(f"Lightning Module must have a method named '{self.method_name}'")
        res_ratio_method = pl_module.__getattribute__(self.method_name)
        res_ratio = res_ratio_method()


        # if self._should_stop(res_ratio):
        #     trainer.should_stop = True
        #     return

        n_to_log = self.log_first_n_ratios 
        if n_to_log == -1:
            n_to_log = res_ratio.size(0)
        for k, ratio in enumerate(res_ratio[:n_to_log]):
            pl_module.log(f"{self.key_prefix}{k}", ratio, on_epoch=True, on_step=False)
        pl_module.log(f"{self.key_prefix}mean", res_ratio[~res_ratio.isnan()].mean(), on_epoch=True, on_step=False, prog_bar=True)

        if self._should_stop(res_ratio):
            if torch.all(res_ratio.isnan()):
                trainer.should_stop = True
        return