import lightning.pytorch as pl

class EpsilonDifferenceEarlyStopping(pl.Callback):
    def __init__(self, monitor: str, eps: float, patience: int = 3) -> None:
        self.monitor = monitor
        self.eps = eps
        self.patience = patience
        self.streak = 0
        self.old_value = None

    def _should_skip(self, trainer: pl.Trainer) -> bool:
        from pytorch_lightning.trainer.states import TrainerFn
        if trainer.state.fn != TrainerFn.FITTING or trainer.sanity_checking:
            return True
        return False

    def on_train_epoch_end(self, trainer: pl.Trainer, pl_module: pl.LightningModule) -> None:
        if self._should_skip(trainer):
            return

        current_value = trainer.callback_metrics.get(self.monitor)
        if current_value is None:
            return

        current_value = current_value.squeeze()
        if self.old_value is not None and (current_value - self.old_value).abs() <= self.eps:
            self.streak += 1
            if self.streak >= self.patience:
                trainer.should_stop = True
        else:
            self.streak = 0
        self.old_value = current_value