import lightning.pytorch as pl

class CoordinatesLoggerCallback(pl.Callback):
    def __init__(self,
                 log_first_n_coords: int = 3,
                 key_prefix: str = "coords_",
                 method_name: str = "get_coordinates") -> None:
        super().__init__()
        self.log_first_n_coords = log_first_n_coords
        self.key_prefix = key_prefix
        self.method_name = method_name

    def _should_skip(self, trainer: pl.Trainer) -> bool:
        from pytorch_lightning.trainer.states import TrainerFn
        if trainer.state.fn != TrainerFn.FITTING or trainer.sanity_checking:
            return True
        return False

    def on_train_epoch_end(self, trainer: pl.Trainer, pl_module: pl.LightningModule) -> None:
        if self._should_skip(trainer):
            return

        if self.method_name not in dir(pl_module):
            raise Exception(f"Lightning Module must have a method named '{self.method_name}'")
        coords_method = pl_module.__getattribute__(self.method_name)
        multi_coords = coords_method()
        multi_coords_to_log = multi_coords[:self.log_first_n_coords]

        for i, coords in enumerate(multi_coords_to_log):
            for j, c in enumerate(coords):
                pl_module.log(f"{self.key_prefix}{i}_{j}", c, on_epoch=True, on_step=False, prog_bar=False)
        return