import numpy as np
import matplotlib
from numpy.typing import ArrayLike
from typing import Callable, Optional, Tuple, Any


def plot_arrows(ax: matplotlib.pyplot.Axes, xs: np.ndarray, ys: np.ndarray, label=None, **kwargs):
    dxs = xs[1:] - xs[:-1]
    dys = ys[1:] - ys[:-1]

    if label is not None:
        ax.arrow(xs[0], ys[0], dxs[0], dys[0], width=.01,
                 length_includes_head=True, label=label, **kwargs)

    zipped = zip(xs, ys, dxs, dys) if label is None else zip(xs[1:], ys[1:], dxs[1:], dys[1:])
    for x, y, dx, dy in zipped:
        ax.arrow(x, y, dx, dy, width=.01, length_includes_head=True, **kwargs)


def get_mM_around_trajectory(
        ws: np.ndarray, 
        padding: float = .5,
        indices: ArrayLike = [0, -1]) -> Tuple[Tuple[float, float], Tuple[float, float]]:
    x, y = indices
    mx, Mx = np.amin(ws[..., x]) - padding, np.amax(ws[..., x]) + padding
    my, My = np.amin(ws[..., y]) - padding, np.amax(ws[..., y]) + padding
    return ((mx, Mx), (my, My))


def contour_loss(
        ax: matplotlib.pyplot.Axes,
        loss_func: Callable,
        mM: Tuple[Tuple[float, float], Tuple[float, float]],
        contour_levels: int = 15,
        fill: bool = True) -> Any:
    (mx, Mx), (my, My) = mM
    divsX = int((Mx - mx) * 10) + 1
    divsY = int((My - my) * 10) + 1

    xv, yv = np.meshgrid(np.linspace(mx, Mx, divsX), np.linspace(my, My, divsY))
    zv = np.stack([xv, yv], axis=-1).reshape(divsX * divsY, 2)
    zs = loss_func(zv).reshape(divsY, divsX)

    if fill:
        return ax.contourf(xv, yv, zs, contour_levels, cmap=matplotlib.cm.magma_r)
    else:
        return ax.contour(xv, yv, zs, contour_levels, cmap=matplotlib.cm.magma)


def plot_minimizer(ax: matplotlib.pyplot.Axes, minimizer: np.ndarray):
    ax.scatter(minimizer[0], minimizer[1], marker='X', color='black', alpha=.8)


def plot_trajectories(
        ax: matplotlib.pyplot.Axes,
        ws: np.ndarray,
        minimizer: Optional[np.ndarray] = None):
    """Plots a trajectory using arrows.

    Args:
        ax : matplotlib.pyplot.Axes
            The axis on which to plot the loss contours and the trajectory.
        ws : np.ndarray
            The trajectories with shape (n_trajs, n_iter, n_dim=2).
        minimizer: Optional[array_like]
            The coordinates on where to plot the minimizer. If None, no minimizer
             will be plotted (default None).
    """
    if ws.ndim != 3:
        ws = np.expand_dims(ws, 0)
    cmap = matplotlib.colormaps['hsv']
    ks = np.random.choice(np.linspace(0, .95, ws.shape[0]), ws.shape[0], replace=False)
    for k, traj in zip(ks, ws):
        plot_arrows(ax, traj[:, 0], traj[:, 1], cmap(k))

    if minimizer is not None:
        plot_minimizer(ax, minimizer)
    ax.set_aspect('equal', 'box')


def plot_trajectory_and_loss_contour(
        ax: matplotlib.pyplot.Axes,
        ws: np.ndarray,
        loss_func: Callable,
        minimizer: np.ndarray = None,
        fill: bool = True):
    """Plots a trajectory using arrows on the loss landscape.

    Args:
        ax : matplotlib.pyplot.Axes
            The axis on which to plot the loss contours and the trajectory.
        ws : np.ndarray
            The trajectories with shape (n_trajs, n_iter, n_dim=2).
        loss_func : Callable
            The loss function.
        minimizer: array_like
            the coordinates on where to plot the minimizer (default [0, 0]).
        fill : bool
            If True, it will plot a filled contour ; otherwise just contour lines (default True).
    """

    mM = get_mM_around_trajectory(ws)
    contour_loss(ax, loss_func, mM, fill=fill)
    plot_trajectories(ax, ws, minimizer=minimizer)