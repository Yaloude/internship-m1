import numpy as np
import matplotlib.pyplot as plt
from typing import Tuple, Optional

def plot_rr(
        ax: plt.Axes,
        rr: np.ndarray,
        quantiles: Tuple[float, float] = (.25, .75),
        logscale: bool = True,
        shared_color: Optional[str] = None,
        *args, **kwargs) -> None:
    """Plots the residual ratio over time on the axis by taking the median w.r.t. time.

    Args:
        ax : matplotlib.pyplot.Axes
            The axis on which to plot the loss contours and the trajectory.
        rr : np.ndarray
            The residuals ratio to plot with shape (n_trajs, n_iter).
        quantiles: Tuple[float, float]
            The quantiles to plot around the median line (default: (.25, .75)).
        logscale: bool
            If True, the residuals ratio will be plotted using logscale on the y axis ; otherwise they won't (default: True).
        shared_color: Optional[str]
            Shared color used for the median line and quantiles fill between."""
    median_rr = np.median(rr, axis=0)
    quants = np.sort(quantiles)
    lower_quantiles = np.quantile(rr, quants[0], axis=0)
    upper_quantiles = np.quantile(rr, quants[1], axis=0)

    if logscale:
        median_rr = np.log10(median_rr)
        lower_quantiles = np.log10(lower_quantiles)
        upper_quantiles = np.log10(upper_quantiles)

    ax.fill_between(np.arange(median_rr.size), lower_quantiles, upper_quantiles, color=shared_color, alpha=.3)
    ax.plot(median_rr, color=shared_color, **kwargs)