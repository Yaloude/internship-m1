import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from typing import Tuple
from numpy.typing import NDArray

def draw_alpha_levels(
        ax: plt.Axes,
        center: Tuple[float, float],
        eigen_values: Tuple[float, float],
        alphas: NDArray[np.float64],
        angle: float = 0,
        *args, **kwargs):
    """Plots alpha-levels.
    
    Args:
        ax : matplotlib.pyplot.Axes
            The axis on which to plot the loss contours and the trajectory.
        center : Tuple[float, float]
            Center of the alpha-levels.
        eigen_values: Tuple[float, float]
            Eigen values of the quadratic objective.
        alphas: array_like
            The alphas values that will used to plot alpha-levels.
        angle: float
            Rotation angle, in degrees and counter clock-wise, of the alpha-levels (default: 0)."""
    for k, alpha in enumerate(alphas):
        w, h = np.sqrt(alpha/eigen_values) * 2
        if "label" in kwargs and k != 0 and kwargs["label"][0] != '_':
            kwargs["label"] = '_' + kwargs["label"][0]
        ell = matplotlib.patches.Ellipse(center, w, h, angle=angle, fill=False, *args, **kwargs)
        ax.add_patch(ell)