import numpy as np
import matplotlib.pyplot as plt

def plot_semi_line(ax: plt.Axes, xy1: np.ndarray, xy2: np.ndarray, *args, **kwargs) -> None:
    """Plot a semi-line (ray) starting from :param xy1: and passing through :param xy2:.
    """
    assert np.all(np.isclose(xy1, xy2)) == False
    direction = [xy2[0] - xy1[0], xy2[1] - xy1[1]]
    xm, xM = ax.get_xlim()
    ym, yM = ax.get_ylim()

    extreme_x, extreme_y = None, None
    if direction[0] != 0:
        extreme_x = xM if direction[0] > 0 else xm
        alpha = (extreme_x - xy1[0]) / direction[0]
        extreme_y = alpha * direction[1] + xy1[1]
    else:
        extreme_x = xy1[0]
        extreme_y = ym if direction[1] < 0 else yM

    ax.set_xlim(xm, xM)
    ax.set_ylim(ym, yM)
    ax.plot((xy1[0], extreme_x), (xy1[1], extreme_y), *args, **kwargs)